package config

import(
	//"github.com/go-ini/ini"
	"os"
	"io/ioutil"
	"encoding/json"
	"log"
)
var (
	// AppConfig 应用程序配置
	InnerConfig = &innerConfig{}
)

type innerConfig struct {
	// LogPath      string `json:"log_path"`              // /data/log/beego.log
	// LogShowColor bool   `json:"log_show_color,string"` //日志是否显示颜色
	// Level        int    `json:"level,string"`          //日志等级
	ListenPort   string `json:"listen_port"`           //端口
}

func LoadConfigINI(fpath string) bool {
	configfile, err := os.Open(fpath)
	if err != nil {
		//model.Log.Errorf("open config file failed, %v", err)
		log.Printf("open config file failed, %v", err)
		return false
	}
	defer configfile.Close()
	data, err := ioutil.ReadAll(configfile)

	if err = json.Unmarshal(data, InnerConfig); err != nil {
		log.Printf("Unmarshal config fail, %v", err)
		return false
	}
	return true
}
